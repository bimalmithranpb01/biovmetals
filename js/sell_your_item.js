var toggler = document.getElementsByClassName("caret");
var i;

for (i = 0; i < toggler.length; i++) {
    toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
    });
}

function activateStepper(count) {
    $(`#stepperList>li:nth-child(${parseInt(count) - 1})>vr`).addClass('active');
    $(`#stepperList>li:nth-child(${count})`).addClass('active');
}