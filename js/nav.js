var _targetId = false;

function showMenu(callerId = false, targetId = false) {
    $(`.nav>.nav-item`).css('border-bottom', 'none');
    $(`.biov-nav-menu`).css('display', 'none');

    if (_targetId !== targetId) {
        _targetId = targetId;

        $(`#${callerId}`).parent().css({
            'border-bottom': '5px solid #b63230'
        });
        $(`#${targetId}`).css({
            'display': 'flex',
            'flex-wrap': 'wrap'
        });
    } else {
        _targetId = false;
    }
}